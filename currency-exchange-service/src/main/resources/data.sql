insert into exchange_value(id, currency_from, currency_to, conversion_multiple, port)
values (10001, 'THB', 'USD', 65, 0);

insert into exchange_value(id, currency_from, currency_to, conversion_multiple, port)
values (10002, 'THB', 'EUR', 75, 0);

insert into exchange_value(id, currency_from, currency_to, conversion_multiple, port)
values (10003, 'THB', 'AUD', 85, 0);
